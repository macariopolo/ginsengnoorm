package ginseng.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Broker {
	protected String url, user, pwd;
	private ConcurrentLinkedQueue<GinsengConnection> libres, ocupadas;
	
	protected Broker() {
		url="jdbc:mysql://alarcosj.esi.uclm.es:3306/ginseng_games?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&useSSL=false";
		user="ideas";
		pwd="ideas123";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			if (DAOConfiguration.pool==Pool.POOL) {
				libres=new ConcurrentLinkedQueue<>();
				ocupadas=new ConcurrentLinkedQueue<>();
				
				GinsengConnection.url=url;
				GinsengConnection.user=user;
				GinsengConnection.pwd=pwd;
				
				for (int i=1; i<60; i++) {
					GinsengConnection pooledConnection=new GinsengPooledConnection(this);
					this.libres.offer(pooledConnection);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	private static class BrokerHolder {
		static Broker singleton=new Broker();
	}
	
	public static Broker get() {
		return BrokerHolder.singleton;
	}

	public GinsengConnection getBd() throws SQLException {
		if (DAOConfiguration.pool==Pool.NO_POOL) {
			Connection bd=DriverManager.getConnection(url, user, pwd);
			return new GinsengNonPooledConnection(bd);
		} else {
			GinsengConnection bd=this.libres.poll();
			this.ocupadas.offer(bd);
			return bd;
		}
	}
	
	public void close(GinsengPooledConnection bd) {
		this.ocupadas.remove(bd);
		this.libres.offer(bd);
	}
}
