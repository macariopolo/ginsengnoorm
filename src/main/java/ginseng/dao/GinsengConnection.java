package ginseng.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class GinsengConnection {
	public static String url;
	public static String user;
	public static String pwd;

	public abstract PreparedStatement prepareStatement(String sql) throws SQLException;

	public abstract void close() throws SQLException;

}
