package ginseng.dao.noSpring.noPool;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;

import ginseng.dao.Broker;
import ginseng.dao.GinsengConnection;
import ginseng.domain.Board;

public class GameDAO {
	
	public static Board loadRandomBoard(Class<?> boardClazz) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="select count(*) from board where type=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, boardClazz.getSimpleName());
			ResultSet rs=ps.executeQuery();
			rs.next();
			int n=rs.getInt(1);
			int index=new Random().nextInt(n);
			ps.close();
			sql="select id from board where type=?";
			ps=bd.prepareStatement(sql);
			ps.setString(1, boardClazz.getSimpleName());
			rs=ps.executeQuery();
			for (int i=0; i<index; i++)
				rs.next();
			Board board=(Board) boardClazz.newInstance();
			return board.load(rs.getString(1));
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static void update(Board board) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="update board set where id=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, board.get_id().toString());
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static void insert(String boardType, Board board, int rows) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="insert into board (id, type, content, rows) values (?, ?, ?, ?)";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, board.get_id().toString());
			ps.setString(2, boardType);
			ps.setString(3, board.getContent());
			ps.setInt(4, rows);
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

}
