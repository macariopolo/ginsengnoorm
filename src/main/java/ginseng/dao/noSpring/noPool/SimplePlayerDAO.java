package ginseng.dao.noSpring.noPool;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import ginseng.dao.Broker;
import ginseng.dao.GinsengConnection;
import ginseng.domain.AbstractPlayer;
import ginseng.domain.SimplePlayer;

public class SimplePlayerDAO {

	public static void insert(AbstractPlayer player) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="insert into abstractplayer (user_name) values (?)";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, player.getUserName());
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static AbstractPlayer identify(String userName) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="select user_name from abstractplayer where user_name=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, userName);
			ResultSet rs=ps.executeQuery();
			if (rs.next()) {
				AbstractPlayer player=new SimplePlayer();
				player.setUserName(userName);
				return player;
			}
			throw new Exception("El usuario no existe");
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static void update(AbstractPlayer player) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="update abstractplayer set victories=?, defeats=?, withdrawals=? where user_name=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setInt(1, player.getVictories());
			ps.setInt(2, player.getDefeats());
			ps.setInt(3, player.getWithdrawals());
			ps.setString(4, player.getUserName());
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}
}
