package ginseng.dao.noSpring.noPool;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import ginseng.dao.Broker;
import ginseng.dao.GinsengConnection;
import ginseng.domain.Token;

public class TokenDAO {

	public static void insert(Token token) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="insert into token (id, user_name, valid_time) values (?, ?, ?)";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, token.getId());
			ps.setString(2, token.getUserName());
			ps.setLong(3, token.getValidTime());
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static void find(String userName, String idToken) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="select valid_time from token where id=? and user_name=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, idToken);
			ps.setString(2, userName);
			ResultSet rs=ps.executeQuery();
			if (rs.next()) {
				long validTime=rs.getLong(1);
				if (validTime<System.currentTimeMillis())
					throw new Exception("Token expirado o token no existe");
			} else throw new Exception("Token expirado o token no existe");
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static void delete(int idToken) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="delete from token where id=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setLong(1, idToken);
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}
}
