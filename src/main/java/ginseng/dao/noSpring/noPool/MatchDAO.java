package ginseng.dao.noSpring.noPool;

import java.sql.PreparedStatement;

import ginseng.dao.Broker;
import ginseng.dao.GinsengConnection;
import ginseng.domain.AbstractPlayer;
import ginseng.domain.Match;

public class MatchDAO {

	public static void insert(Match match) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="insert into matchs (id, player_a, player_b, finished, id_board) values (?, ?, ?, ?, ?)";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, match.getIdMatch());
			ps.setString(2, match.getPlayerA().getUserName());
			ps.setString(3, match.getPlayerB().getUserName());
			ps.setBoolean(4, false);
			ps.setString(5, match.getBoard().get_id());
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static void insertMovement(Match match, AbstractPlayer player, Integer[] coordinates) throws Exception {
		String sCoordinates="";
		for (int i=0; i<coordinates.length-1; i++)
			sCoordinates+=coordinates[i]+",";
		sCoordinates+=coordinates[coordinates.length-1];
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="insert into movement (id_match, player, coordinates) values (?, ?, ?)";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, match.getIdMatch());
			ps.setString(2, player.getUserName());
			ps.setString(3, sCoordinates);
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static void updateWinner(Match match) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="update matchs set winner=?, finished=true where id=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, match.getWinnerName());
			ps.setString(2, match.getIdMatch());
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}
}
