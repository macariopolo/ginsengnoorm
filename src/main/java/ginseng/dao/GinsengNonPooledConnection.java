package ginseng.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class GinsengNonPooledConnection extends GinsengConnection {
	private Connection bd;

	public GinsengNonPooledConnection(Connection bd) {
		this.bd=bd;
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return this.bd.prepareStatement(sql);
	}

	@Override
	public void close() throws SQLException {
		this.bd.close();
	}

}
