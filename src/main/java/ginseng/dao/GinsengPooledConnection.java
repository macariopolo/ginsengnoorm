package ginseng.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class GinsengPooledConnection extends GinsengConnection {
	private Broker broker;
	private Connection bd;

	public GinsengPooledConnection(Broker broker) throws SQLException {
		this.broker=broker;
		this.bd=DriverManager.getConnection(url, user, pwd);
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return this.bd.prepareStatement(sql);
	}

	@Override
	public void close() throws SQLException {
		this.broker.close(this);
	}

}
