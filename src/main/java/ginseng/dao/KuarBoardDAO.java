package ginseng.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import ginseng.domain.Board;
import ginseng.domain.kuar.KuarBoard;

public class KuarBoardDAO {

	public static Board load(String id) throws Exception {
		GinsengConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="select times_played, best_movements, rows, content from board where id=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs=ps.executeQuery();
			KuarBoard board=new KuarBoard();
			if (rs.next()) {
				board.set_id(id);
				board.setTimesPlayed(rs.getInt(1));
				board.setBestTime(rs.getInt(2));
				board.setRows(rs.getInt(3));
				board.setContent(rs.getString(4));
			}
			return board;
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

}
