package ginseng.domain;

public abstract class AbstractPlayer {
	protected String userName;
	protected Match currentMatch;
	protected int victories;
	protected int defeats;
	protected int withdrawals;
		
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public void setCurrentMatch(Match match) {
		this.currentMatch=match;
	}
	
	public Match getCurrentMatch() {
		return currentMatch;
	}

	public void increaseVictoriesDefeatsWithdrawals(int victories, int defeats, int withdrawals) throws Exception {
		this.victories+=victories;
		this.defeats+=defeats;
		this.withdrawals+=withdrawals;
		update();
	}

	protected abstract void update() throws Exception;

	public int getVictories() {
		return victories;
	}
	
	public int getDefeats() {
		return defeats;
	}
	
	public int getWithdrawals() {
		return withdrawals;
	}
}
