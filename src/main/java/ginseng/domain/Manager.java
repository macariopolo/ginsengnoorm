package ginseng.domain;

import java.util.Enumeration;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import ginseng.dao.noSpring.noPool.MatchDAO;
import ginseng.domain.kuar.KuarGame;
import ginseng.domain.ppt.PPTGame;
import ginseng.domain.tictactoe.TictactoeGame;

public class Manager {
	private ConcurrentHashMap<Integer, Game> games;
	private ConcurrentHashMap<String, AbstractPlayer> players;
	protected ConcurrentHashMap<String, Match> inPlayMatches;
	
	private Manager() {
		this.inPlayMatches=new ConcurrentHashMap<>();
		games=new ConcurrentHashMap<>();
		Game s3x3=new KuarGame(3);
		games.put(1, s3x3);
		Game s4x4=new KuarGame(4);
		games.put(2, s4x4);
		Game s5x5=new KuarGame(5);
		games.put(3, s5x5);

		Game tictactoe=new TictactoeGame();
		games.put(10, tictactoe);
		Game ppt=new PPTGame();
		games.put(11, ppt);

		this.players=new ConcurrentHashMap<>();
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	public static Manager get() {
		return ManagerHolder.singleton;
	}

	public Match joinGame(AbstractPlayer player, int idGame) throws Exception {
		if (this.players.get(player.getUserName())==null)
			throw new Exception("User not logged");
		Game game=this.games.get(idGame);
		Match match = game.getMatch(player);
		if (match.isComplete())
			game.pendingMatches.remove(match.getIdMatch());
		return match;
	}
	
	public Match joinGame(AbstractPlayer player, String gameName) throws Exception {
		Enumeration<Game> games=this.games.elements();
		while (games.hasMoreElements()) {
			Game game=games.nextElement();
			if (game.getName().equals(gameName)) {
				Match match = game.getMatch(player);
				if (match.isComplete()) {
					game.pendingMatches.remove(match.getIdMatch());
				}
				return match;
			}
		}
		return null;
	}

	public Match move(String idMatch, AbstractPlayer player, JSONArray coordinates) throws Exception {
		Integer[] iC=new Integer[coordinates.length()];
		for (int i=0; i<iC.length; i++)
			iC[i]=coordinates.getInt(i);
		Match match=this.inPlayMatches.get(idMatch);
		match.move(player, iC);
		return match;
	}

	public JSONObject logout(AbstractPlayer player) throws Exception {
		JSONObject jso=new JSONObject();
		this.userLeaves(player.getUserName());
		return jso;
	}

	public AbstractPlayer login(String userName, String pwd) throws Exception {
		if (userName.length()==0 || pwd.length()==0)
			throw new Exception("Credenciales inválidas");
		AbstractPlayer player=Player.identify(userName, pwd);
		if (player==null)
			throw new Exception("Credenciales inválidas");
		this.players.put(userName, player);
		return player;
	}

	public AbstractPlayer login(String userName) throws Exception {
		AbstractPlayer player=SimplePlayer.identify(userName);
		if (player==null)
			throw new Exception("Credenciales inválidas");
		this.players.put(userName, player);
		return player;
	}
	
	public AbstractPlayer register(String email, String userName, String pwd1, String pwd2) throws Exception {
		if (!pwd1.equals(pwd2))
			throw new Exception("Error: las contraseñas no coinciden");
		AbstractPlayer player=Player.insert(email, userName, pwd1);
		return player;
	}

	public Token requestToken(String userName) throws Exception {
		Token token=new Token(userName);
		token.insert();
		return token;
	}

	public void resetPwd(String userName, String pwd, String idToken) throws Exception {
		Token.load(userName, idToken);
		Player.updatePwd(userName, pwd);
	}

	public AbstractPlayer simpleRegister(String userName) throws Exception {
		AbstractPlayer player=SimplePlayer.insert(userName);
		return player;
	}

	public void addInPlayMatch(Match match) {
		inPlayMatches.put(match.getIdMatch(), match);
	}

	public void removeInPlayMatch(String id) {
		inPlayMatches.remove(id);
	}
	
	public ConcurrentHashMap<String, Match> getInPlayMatches() {
		return inPlayMatches;
	}

	public AbstractPlayer leaveMatch(String idMatch, String userName) throws Exception {
		Match match=getInPlayMatches().get(idMatch);
		removeInPlayMatch(idMatch);
		
		AbstractPlayer winner, looser;
		if (match.getPlayerA().getUserName().equals(userName)) {
			winner=match.getPlayerB();
			looser=match.getPlayerA();
		} else {
			winner=match.getPlayerA();
			looser=match.getPlayerB();
		}
		winner.increaseVictoriesDefeatsWithdrawals(1, 0, 0);
		looser.increaseVictoriesDefeatsWithdrawals(0, 1, 1);
		winner.setCurrentMatch(null);
		looser.setCurrentMatch(null);
		match.setWinner(winner);
		MatchDAO.updateWinner(match);
		return winner;
	}

	public void userLeaves(String userName) throws Exception {
		AbstractPlayer player = this.players.remove(userName);
		if (player.getCurrentMatch()!=null)
			this.leaveMatch(player.getCurrentMatch().getIdMatch(), userName);
	}

	public Game findGame(int idGame) {
		return this.games.get(idGame);
	}
}
