package ginseng.domain;

import org.json.JSONObject;

public abstract class Board {
	protected String _id;
	protected int timesPlayed;
	protected int bestTime;
	protected int bestMovements;
	protected Match match;
	
	public Board() {
	}
	
	public Board(Match match) {
		this.match=match;
		this.bestTime=Integer.MAX_VALUE;
		this.bestMovements=Integer.MAX_VALUE;
	}
	
	protected abstract void move(AbstractPlayer player, Integer[] coordinates) throws Exception;		

	public abstract AbstractPlayer getWinner();
	public abstract boolean end();
	public abstract String getContent();

	public JSONObject toJSON() throws Exception {
		throw new Exception("toJSON not implemented in " + this.getClass().getSimpleName());
	}
	
	public void set_id(String _id) {
		this._id = _id;
	}
	
	public String get_id() {
		return _id;
	}
	
	public void setMatch(Match match) {
		this.match = match;
	}
	
	public void setTimesPlayed(int timesPlayed) {
		this.timesPlayed = timesPlayed;
	}

	public void increaseTimesPlayed() {
		this.timesPlayed++;
	}
	
	public int getBestMovements() {
		return bestMovements;
	}
	
	public void setBestMovements(int bestMovements) {
		this.bestMovements = bestMovements;
	}
	
	public int getBestTime() {
		return bestTime;
	}
	
	public void setBestTime(int bestTime) {
		this.bestTime = bestTime;
	}

	public Board load(String id) throws Exception {
		throw new Exception("This board type cannot be loaded from the DB");
	}
}
