package ginseng.domain.kuar;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import ginseng.dao.KuarBoardDAO;
import ginseng.dao.noSpring.noPool.GameDAO;
import ginseng.domain.Board;
import ginseng.domain.Game;
import ginseng.domain.Match;

public class KuarGame extends Game {
	private int rows;

	public KuarGame(int rows) {
		this.rows=rows;
	}

	@Override
	public String getName() {
		return "Kuar " + rows + "x" + rows;
	}

	@Override
	protected Match createMatch() {
		Board board=getRandomBoard(true);
		Match match = new KuarMatch(this);
		match.setBoard(board);
		return match;
	}
	
	@Override
	public Board findBoard(String idBoard) throws Exception {
		return KuarBoardDAO.load(idBoard);
	}

	private Board getRandomBoard(boolean testingMode) {
	    String contenido=null;
	    Board board=null;
	    
	    if (testingMode) {
	    		try {
	    			String id="5c20a4ec4034f8ea0c53dc96";
	    			if (rows==4)
	    				id="5c20ad394034f8ea695cd5d0";
	    			board=KuarBoardDAO.load(id);
				if (board!=null) {
					contenido=board.getContent();
					return board;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
	    
		if (new Random().nextBoolean()) {
			try {
				board=GameDAO.loadRandomBoard(KuarBoard.class);
				if (board!=null)
					contenido=board.getContent();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		
		if (contenido==null) {	
			double percentage=0.8;
		    int total=(int) ((rows*rows)*percentage);
		    ArrayList<Pair> selCoords=new ArrayList<Pair>();
		    int n=0;
		    Random dado=new Random();
		    int dadoRow, dadoCol;
		    int[][] squares=new int[rows][rows];
		    while (n<total) {
		        Pair pair=new Pair();
		        do {
		            dadoRow = dado.nextInt(rows);
		            dadoCol = dado.nextInt(rows);
		            pair.row=dadoRow; pair.col=dadoCol;
		        } while (selCoords.contains(pair));
		        selCoords.add(pair);
		        n++;
		        squares[dadoRow][dadoCol]=n;
		    }
		    contenido="";
		    for (int i=0; i<rows; i++)
			    	for (int j=0; j<rows; j++)
			    		contenido+=squares[i][j] + ",";
		    
		    contenido=contenido.substring(0, contenido.length()-1);
		    String idBoard=UUID.randomUUID().toString();
			board=new KuarBoard(idBoard, rows, contenido);
			try {
				GameDAO.insert(KuarBoard.class.getSimpleName(), board, rows);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return board;
	}
}
