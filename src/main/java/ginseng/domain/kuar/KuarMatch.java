package ginseng.domain.kuar;

import ginseng.domain.AbstractPlayer;
import ginseng.domain.Board;
import ginseng.domain.Game;
import ginseng.domain.Match;

public class KuarMatch extends Match {
	private KuarBoard boardA, boardB;
	
	public KuarMatch(Game game) {
		super(game);
	}

	@Override
	protected boolean tieneElTurno(AbstractPlayer player) {
		return true;
	}
	
	public KuarBoard getBoardA() {
		return boardA;
	}
	
	public KuarBoard getBoardB() {
		return boardB;
	}
	
	@Override
	public void setBoard(Board board) {
		super.setBoard(board);
		KuarBoard theBoard=(KuarBoard) board;
		this.boardA=new KuarBoard(theBoard.get_id(), theBoard.getRows(), theBoard.getContent());
		this.boardB=new KuarBoard(theBoard.get_id(), theBoard.getRows(), theBoard.getContent());
		this.boardA.setMatch(this);
		this.boardB.setMatch(this);
	}

	@Override
	public void calculateFirstPlayer() {
		// TODO Auto-generated method stub
		
	}
}
