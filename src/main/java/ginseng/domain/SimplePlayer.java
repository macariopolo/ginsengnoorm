package ginseng.domain;

import ginseng.dao.noSpring.noPool.SimplePlayerDAO;

public class SimplePlayer extends AbstractPlayer {
	public SimplePlayer() {
	}
		
	
	public Match move(Integer[] coordinates) throws Exception {
		return this.currentMatch.move(this, coordinates);
	}

	public static SimplePlayer insert(String userName) throws Exception {
		SimplePlayer player=new SimplePlayer();
		player.setUserName(userName);
		SimplePlayerDAO.insert(player);
		return player;
	}

	public static AbstractPlayer identify(String userName) throws Exception {
		return SimplePlayerDAO.identify(userName);
	}


	@Override
	protected void update() throws Exception {
		SimplePlayerDAO.update(this);
	}
}
