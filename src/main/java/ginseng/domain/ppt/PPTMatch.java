package ginseng.domain.ppt;

import ginseng.domain.AbstractPlayer;
import ginseng.domain.Game;
import ginseng.domain.Match;

public class PPTMatch extends Match {
	
	public PPTMatch(Game game) {
		super(game);
		this.board=new PPTBoard(this);
	}

	@Override
	public void calculateFirstPlayer() {
		// TODO Auto-generated method stub

	}

	@Override
	protected boolean tieneElTurno(AbstractPlayer player) {
		return true;
	}

}
