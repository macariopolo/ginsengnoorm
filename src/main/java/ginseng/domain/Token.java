package ginseng.domain;

import java.util.Random;
import java.util.UUID;

import ginseng.dao.noSpring.noPool.TokenDAO;

public class Token {
	private String id;
	private Long validTime;
	private String userName; 
	
	public Token(String userName) {
		this.userName=userName;
		this.id=UUID.randomUUID().toString();
		this.validTime=System.currentTimeMillis() + 10*60*1000;
	}
	
	public void insert() throws Exception {
		TokenDAO.insert(this);
	}
	
	public String getId() {
		return id;
	}

	public String getUserName() {
		return userName;
	}
	
	public long getValidTime() {
		return validTime;
	}

	public static void load(String userName, String idToken) throws Exception {
		TokenDAO.find(userName, idToken);
	}

	public static void delete(int id) throws Exception {
		TokenDAO.delete(id);
	}
}
