package ginseng.domain;

import ginseng.dao.noSpring.noPool.PlayerDAO;
import ginseng.dao.noSpring.noPool.SimplePlayerDAO;

public class Player extends AbstractPlayer {
	private String email;
	
	public Player() {
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public static Player insert(String email, String userName, String pwd) throws Exception {
		Player player=new Player();
		player.setEmail(email);
		player.setUserName(userName);
		PlayerDAO.insert(player, pwd);
		return player;
	}

	public static Player identify(String userName, String pwd) throws Exception {
		return PlayerDAO.identify(userName, pwd);
	}

	public static Player find(String userName) throws Exception {
		return PlayerDAO.find(userName);
	}

	public static void updatePwd(String userName, String pwd) throws Exception {
		PlayerDAO.updatePwd(userName, pwd);
	}

	@Override
	protected void update() throws Exception {
		SimplePlayerDAO.update(this);
	}

}
